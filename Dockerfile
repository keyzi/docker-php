FROM php:8.2-fpm

# Install xdebug
RUN yes | pecl install xdebug \
	&& echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
	&& echo "xdebug.mode = debug" >> /usr/local/etc/php/conf.d/xdebug.ini \
	&& echo "xdebug.start_with_request = yes" >> /usr/local/etc/php/conf.d/xdebug.ini \
	&& echo "xdebug.client_port = 9000" >> /usr/local/etc/php/conf.d/xdebug.ini \
	&& rm -rf /tmp/pear

RUN apt-get update

RUN apt-get install -y git bash

# Install composer
RUN curl -sSL https://getcomposer.org/installer | php \
	&& chmod +x composer.phar \
	&& mv composer.phar /usr/local/bin/composer

# Install Symfony CLI
# RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv $HOME/.symfony5/bin/symfony /usr/local/bin/symfony

# Opcache
RUN docker-php-ext-install opcache

# ZIP
RUN apt-get install -y libzip-dev zip \
	# --with-zip e doar in php7.4
	# && docker-php-ext-configure zip --with-zip \
	&& docker-php-ext-configure zip \
	&& docker-php-ext-install zip

# BCMath
RUN docker-php-ext-install bcmath && docker-php-ext-enable bcmath

# Memcached
RUN apt-get -y update \
&& apt-get install -y libmemcached-dev \
&& pecl install memcached \
&& docker-php-ext-enable memcached

# Intl
RUN apt-get -y update \
&& apt-get install -y libicu-dev \
&& docker-php-ext-configure intl \
&& docker-php-ext-install intl

# Image Processing (ImageMagick) & gd
RUN apt-get update && apt-get install -y \
			libfreetype6-dev \
			libjpeg62-turbo-dev \
			libpng-dev \
			libmagickwand-dev \
			libmemcached-dev \
			libwebp-dev \
	# php 7.4 
	&& docker-php-ext-configure gd --with-freetype --with-webp --with-jpeg \ 
	&& docker-php-ext-install -j$(nproc) gd \
	&& pecl install imagick && docker-php-ext-enable imagick

# PDO/MySQL
RUN docker-php-ext-install pdo_mysql && docker-php-ext-enable pdo_mysql

# Install Postgre PDO
RUN apt-get install -y libpq-dev \
	&& docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
	&& docker-php-ext-install pdo pdo_pgsql pgsql

RUN docker-php-ext-install exif && docker-php-ext-enable exif

# Install APCu
RUN pecl install apcu \
	&& docker-php-ext-enable apcu \
	&& pecl clear-cache

# Install WebKit HTML to PDF
RUN curl -sSL https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6.1-2/wkhtmltox_0.12.6.1-2.bullseye_amd64.deb -O
RUN apt-get install -y ./wkhtmltox_0.12.6.1-2.bullseye_amd64.deb

# Copy phpconfig.ini
ADD ./php/* "$PHP_INI_DIR/conf.d/"

# Setup User www-data with id 1000
RUN apt-get install -y sudo

ARG UNAME=www-data
ARG UGROUP=www-data
ARG UID=1000
ARG GID=1000
RUN usermod --uid $UID $UNAME
RUN groupmod --gid $GID $UGROUP

RUN mkdir /var/www/home
RUN chown -R $UNAME:$UGROUP /var/www/home
RUN usermod -d /var/www/home --shell /bin/bash $UNAME
RUN adduser $UNAME sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

COPY --chown=$UNAME:$UGROUP bashrc.bash /var/www/home/.bashrc

USER www-data
SHELL ["/bin/bash", "--login", "-c"]

# Setup nvm
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash

RUN ls -la /var/www/home

RUN . /var/www/home/.nvm/nvm.sh && nvm install 18
